(function() {
	let fibonacci = () => {
		let cache = [];
		return {
			of: function(pos) {
				if (cache[pos]) {
					return cache[pos];
				} else {
					if (pos < 3) {
						return 1;
					} else {
						cache[pos] = this.of(pos - 1) + this.of(pos - 2);
					}
				}
				return cache[pos];
			}
		};
	};
	console.log(fibonacci().of(50));
	console.log(typeof cache === 'undefined'); // true because it's private
})();
