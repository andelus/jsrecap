(function() {
	// IIFE Local Scope
	Array.prototype.myMap = function(cb) {
		return this.reduce((pre, curr) => {
			pre.push(cb(curr));
			return pre;
		}, []);
	};
	let multiplesOfTwo = [1, 2, 3, 4].myMap(ele => ele * 2);
	console.log(multiplesOfTwo);
})();
