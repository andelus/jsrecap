(function() {
	// IIFE Local Scope
	let addElementsOfAList = list => {
		if (list instanceof Array) {
			return list.reduce((a, b) => a + b, 0);
		} else {
			throw 'addElementsOfAList(list) list not of type array';
		}
	};
	let sum = addElementsOfAList([1, 2, 3, 4]);
	console.log(sum); //10
})();
