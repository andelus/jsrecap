(function() {
	// IIFE Local Scope
	let multiplyElementsOfAList = list => {
		if (list instanceof Array) {
			return list.reduce((a, b) => a * b, 1);
		} else {
			throw 'multiplyElementsOfAList(list) list not of type array';
		}
	};
	let multiplyResult = multiplyElementsOfAList([1, 2, 3, 4]);
	console.log(multiplyResult); //24
})();
